def hanoi_tower(start, middle, goal, n):
    if n == 1:
        print(start,"->",goal)
        return 
    hanoi_tower(start, goal, middle, n-1)
    print(start, "->", goal)
    hanoi_tower(middle, start, goal, n-1)

hanoi_tower("A", "B", "C", 4)