import random

page = [[" "," "," "],
        [" "," "," "],
        [" "," "," "]]

#user_input func put the players pieces on the board
def user_input(row, column):
    page[row - 1][column - 1] = "O"

    check = True
    for y in range(0,20):
        AI_row, AI_column = AI()
        if page[AI_row][AI_column] == " " :
            page[AI_row][AI_column] = "X"
            break

    # print(" ",page[0],'\n',page[1],'\n',page[2])
    print("",page[0][0],"|",page[0][1],"|",page[0][2],"\n",page[1][0],"|",page[1][1],"|",page[1][2],"\n",page[2][0],"|",page[2][1],"|",page[2][2],)

#AI func make random number 
def AI ():
    row_AI = random.randint(0,2)
    column_AI = random.randint(0,2)
    return row_AI, column_AI

#check_winer func check the winre of game
def check_winer():
    for rows in page:
        if rows[0] == "O" and rows[1] == "O" and rows[2] == "O":
            return True, "User is winer"
            break 
        elif rows[0] == "X" and rows[1] == "X" and rows[2] == "X":
            return True, "AI is winer" 
            break

    for a in range(0,3):
        if page[0][a] == "O" and page[1][a] == "O" and page[2][a] == "O":
            return True, "User is winer"
            break 
        elif page[0][a] == "X" and page[1][a] == "X" and page[2][a] == "X":
            return True, "AI is winer" 
            break
    
    if page[0][0] == "O" and page[1][1] == "O" and page[2][2] == "O" or page[0][2] == "O" and page[1][1] == "O" and page[2][0] == "O":
        return True, "User is winer"
    elif page[0][0] == "X" and page[1][1] == "X" and page[2][2] == "X" or page[0][2] == "X" and page[1][1] == "X" and page[2][0] == "X":
        return True, "AI is winer"

    return False, ""

i = 0
while i < 5:
    check, winer = check_winer()
    if check == True:
        print(winer)
        i = 7

    row = int(input('Enter row: '))
    column = int(input('Enter column: '))

    user_input(row, column)
    i += 1

if i == 5:
    print("no body is winer")
    
    
    